cp .env.example .env

docker-compose up -d --build
docker-compose exec app php artisan key:generate
docker-compose exec app php artisan migrate
