<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-aFq/bzH65dt+w6FI2ooMVUpc+21e0SRygnTpmBvdBgSdnuTN7QbdgL+OapgHtvPp" crossorigin="anonymous">

    <script src="https://cdn.tailwindcss.com"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <style>
        /*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */
        html {
            line-height: 1.15;
            -webkit-text-size-adjust: 100%
        }

        body {
            margin: 0
        }

        a {
            /* color: gray; */
            text-decoration: none;
        }

        html {
            font-family: system-ui, -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Helvetica Neue, Arial, Noto Sans, sans-serif, Apple Color Emoji, Segoe UI Emoji, Segoe UI Symbol, Noto Color Emoji;
            line-height: 1.5
        }

        .separator {
            color: #5c86a7
        }

        .page-link {
            color: #4d5d53
        }

        .example-wrapper {
            margin: 1em auto;
            max-width: 800px;
            width: 95%;
            font: 18px / 1.5 sans-serif;
        }

        .example-wrapper code {
            background: #F5F5F5;
            padding: 2px 6px;
        }

        /* Dropdown Button */
        .dropbtn {
            background-color: #4CAF50;
            color: white;
            padding: 16px;
            font-size: 16px;
            border: none;
            cursor: pointer;
        }

        /* Dropdown button on hover & focus */
        .dropbtn:hover,
        .dropbtn:focus {
            background-color: #3e8e41;
        }

        /* The search field */
        #myInput {
            border-box: box-sizing;
            background-image: url('searchicon.png');
            background-position: 14px 12px;
            background-repeat: no-repeat;
            font-size: 16px;
            padding: 14px 20px 12px 45px;
            border: none;
            border-bottom: 1px solid #ddd;
        }

        /* The search field when it gets focus/clicked on */
        #myInput:focus {
            outline: 3px solid #ddd;
        }

        /* The container <div> - needed to position the dropdown content */
        .dropdown {
            position: relative;
            display: inline-block;
        }

        /* Dropdown Content (Hidden by Default) */
        .dropdown-content {
            display: none;
            position: absolute;
            background-color: #f6f6f6;
            min-width: 230px;
            border: 1px solid #ddd;
            z-index: 1;
        }

        /* Links inside the dropdown */
        .dropdown-content a {
            color: black;
            padding: 12px 16px;
            text-decoration: none;
            display: block;
        }

        /* Change color of dropdown links on hover */
        .dropdown-content a:hover {
            background-color: #f1f1f1
        }

        /* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
        .show {
            display: block;
        }
    </style>
    <script>
        function filterByCategory() {
            document.getElementById("myDropdown").classList.toggle("show");
        }

        function filterByGender() {
            document.getElementById("myDropdown1").classList.toggle("show");
        }

        function filterByBirthDate() {
            document.getElementById("myDropdown2").classList.toggle("show");
        }

        function filterByAge() {
            document.getElementById("myDropdown3").classList.toggle("show");
        }

        function filterByAgeRange() {
            document.getElementById("myDropdown4").classList.toggle("show");
        }
    </script>

    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
    </style>
</head>

<body class="antialiased">
    <h1>Users Info</h1>
    <button class="dropbtn"><a href="/">Reset filters</a></button>
    <div class="dropdown">
        <button onclick="filterByCategory()" class="dropbtn">Filter By Category</button>
        <div id="myDropdown" class="dropdown-content">
            <a href="/">
                << Back</a>
                    @foreach ($categrories as $category)
                    <a href="{{ Request::getQueryString() != null ? '?'.Request::getQueryString().'&' : '?'}}category={{ $category->category }}">{{ $category->category }}</a>
                    @endforeach
        </div>
    </div>
    <div class="dropdown">
        <button onclick="filterByGender()" class="dropbtn">Filter By Gender</button>
        <div id="myDropdown1" class="dropdown-content">
            <a href="/">
                << Back</a>
                    <a href="{{ Request::getQueryString() != null ? '?'.Request::getQueryString().'&' : '?'}}gender=female">Female</a>
                    <a href="{{ Request::getQueryString() != null ? '?'.Request::getQueryString().'&' : '?'}}gender=male">Male</a>
        </div>
    </div>
    <div class="dropdown">
        <button onclick="filterByBirthDate()" class="dropbtn">Filter By Date of Birth</button>
        <div id="myDropdown2" class="dropdown-content">
            <a href="/">
                << Back</a>
                    @foreach ($birthDates as $birthDate)
                    <a href="{{ Request::getQueryString() != null ? '?'.Request::getQueryString().'&' : '?'}}birthDate=" {{ $birthDate->birthdate }}"">{{ $birthDate->birthdate }}</a>
                    @endforeach
        </div>
    </div>
    <div class="dropdown">
        <button onclick="filterByAge()" class="dropbtn">Filter By Age</button>
        <div id="myDropdown3" class="dropdown-content">
            <a href="/">
                << Back</a>
                    @foreach ($ages as $age)
                    <a href="{{ Request::getQueryString() != null ? '?'.Request::getQueryString().'&' : '?'}}age={{ $age }}">{{ $age }}</a>
                    @endforeach
        </div>
    </div>
    <div class="dropdown">
        <button onclick="filterByAgeRange()" class="dropbtn">Filter By Age Range</button>
        <div id="myDropdown4" class="dropdown-content">
            <a href="/">
                << Back</a>
                    @foreach ($ages as $ageRange)
                    <a href="{{ Request::getQueryString() != null ? '?'.Request::getQueryString().'&' : '?'}}age_from={{ $ageRange }}&age_to={{ $ageRange+5 }}">{{ $ageRange }} - {{ $ageRange + 5 }}</a>
                    @endforeach
        </div>
    </div>

    <button class="dropbtn"><a href="/load/{{ Request::getQueryString() != null ? '?'.Request::getQueryString().'&' : '?'}}">Download CSV</a></button>



    <table class="table table-bordered table-hover">
        <thead>
            <th>Category</th>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Email</th>
            <th>Gender</th>
            <th>birthDate</th>
        </thead>
        <tbody>
            @if ($users->count() == 0)
            <tr>
                <td colspan="5">No users to display.</td>
            </tr>
            @endif

            @foreach ($users as $user)
            <tr>
                <td>{{ $user->category }}</td>
                <td>{{ $user->firstname }}</td>
                <td>{{ $user->lastname }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->gender }}</td>
                <td>{{ $user->birthdate }}</td>

            </tr>
            @endforeach
        </tbody>
    </table>

    {{ $users->links() }}

    <p>
        Displaying {{$users->count()}} of {{ $users->total() }} product(s).
    </p>
</body>

</html>