<?php

namespace App\Service\Upload;

use App\Http\Requests\FilterRequest;
use Illuminate\Pagination\LengthAwarePaginator;
interface UploadServiceInterface
{

    public function categoriesList(): LengthAwarePaginator;
    public function birthDatesList(): LengthAwarePaginator;
    public function agesList(): array;


    public function filterExports(FilterRequest $request);

}
