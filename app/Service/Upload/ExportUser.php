<?php

namespace App\Service\Upload;

use Maatwebsite\Excel\Concerns\FromCollection;

class ExportUser implements FromCollection
{
   private $query;
   public function __construct($query)
   {
      $this->query = $query;
   }
   /**
    * @return \Illuminate\Support\Collection
    */
   public function collection()
   {
      return $this->query;
   }
}
