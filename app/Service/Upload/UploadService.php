<?php

namespace App\Service\Upload;

use App\Http\Requests\FilterRequest;
use App\Models\CsvExport;
use Illuminate\Pagination\LengthAwarePaginator;

class UploadService implements UploadServiceInterface
{
    public function categoriesList(): LengthAwarePaginator
    {
        return CsvExport::select('category')->groupBy('category')->paginate(10);
    }

    public function birthDatesList(): LengthAwarePaginator
    {
        return CsvExport::select('birthdate')->groupBy('birthdate')->paginate(10);
    }

    public function agesList(): array
    {
        return range(10, 120, 5);
    }
    public function filterExports(FilterRequest $request)
    {
        $csv =
            CsvExport::query()->where(function ($query) use ($request) {

                if ($request->has('category')) {
                    $query->where('category', $request->category);
                }

                if ($request->has('birthDate')) {
                    $query->where('birthdate', $request->birthDate);
                }

                if ($request->has('gender')) {
                    $query->where('gender', $request->gender);
                }

                if ($request->has('age')) {
                    $query
                        ->whereRaw("extract(year from AGE(now(),birthdate)) = {$request->age}");
                }


                if ($request->has('age_from')) {
                    $query
                        ->whereRaw("extract(year from AGE(now(),birthdate)) >= {$request->age_from}");
                }

                if ($request->has('age_to')) {
                    $query
                        ->whereRaw("extract(year from AGE(now(),birthdate)) < {$request->age_to}");
                }
            })->paginate(50)->withQueryString();
        return $csv;
    }
}
