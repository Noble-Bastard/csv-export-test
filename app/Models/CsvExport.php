<?php

namespace App\Models;

use App\Domain\Contracts\CsvExportContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CsvExport extends Model
{
    use HasFactory;
    protected $fillable = CsvExportContract::FILLABLE;
}
