<?php

namespace App\Domain\Contracts;

interface CsvExportContract
{
    const NAME = 'firstname';
    const LASTNAME = 'lastname';
    const EMAIL = 'email';
    const GENDER = 'gender';
    const BIRTHDATE = 'birthdate';
    const CATEGORY = 'category';


    const FILLABLE = [
        self::NAME,
        self::LASTNAME,
        self::EMAIL,
        self::GENDER,
        self::BIRTHDATE,
        self::CATEGORY
    ];
}
