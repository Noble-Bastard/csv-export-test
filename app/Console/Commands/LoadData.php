<?php

namespace App\Console\Commands;

use App\Models\CsvExport;
use Carbon\Carbon;
use Illuminate\Console\Command;

class LoadData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:load';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        if ($file = fopen("public/uploads/dataset.txt", "r")) {
            while (!feof($file)) {
                $line = fgets($file);
                $csvImported = array_values(explode(",", $line));
                $csvExport = new CsvExport();
                $csvExport->category = $csvImported[0];
                $csvExport->firstname = $csvImported[1];
                $csvExport->lastname = $csvImported[2];
                $csvExport->email = $csvImported[3];
                $csvExport->gender = $csvImported[4];
                $csvExport->birthdate = Carbon::parse($csvImported[5])->format('Y-m-d');;

                $csvExport->save();
            }
            fclose($file);
        }
        return 0;
    }
}
