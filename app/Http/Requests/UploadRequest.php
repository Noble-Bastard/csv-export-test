<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadRequest extends FormRequest
{
    public function authorize(): bool
    {
        return 1;
    }


    public function rules(): array
    {
        return [
            'file' => 'required'
        ];
    }
}
