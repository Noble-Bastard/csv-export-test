<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return 1;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'nullable',
            'gender' => 'nullable',
            'birthDate' => 'nullable',
            'age' => 'nullable|integer',
            'from_age' => 'nullable|integer',
            'to_age' => 'nullable|integer'
        ];
    }
}
