<?php

namespace App\Http\Controllers;

use App\Http\Requests\FilterRequest;
use App\Http\Requests\UploadRequest;
use App\Models\CsvExport;
use App\Service\Upload\ExportUser;
use App\Service\Upload\UploadService;
use Illuminate\Support\Facades\Response;
use Maatwebsite\Excel\Facades\Excel;

class CsvExportController extends Controller
{
    private $uploadService;

    public function __construct(UploadService $uploadService)
    {
        $this->uploadService = $uploadService;
    }


    public function index(FilterRequest $request)
    {
        return view('welcome')->with(
            [
                'users' => $this->uploadService->filterExports($request),
                'categrories' =>  $this->uploadService->categoriesList(),
                'birthDates' =>  $this->uploadService->birthDatesList(),
                'ages' =>  $this->uploadService->agesList()
            ]
        );
    }


    public function store(FilterRequest $request)
    {
        $csv = $this->uploadService->filterExports($request);
        return Excel::download(new ExportUser($csv), 'users.csv');
    }
}
